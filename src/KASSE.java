/**
 * Created by beb on 2017-03-01.
 */
public class KASSE {
    private KASSENSYMBOL darstellung;
    private int nummer;
    private WARTESCHLANGE warteschlange;
    private KUNDE kunde;

    KASSE(int nummer, WARTESCHLANGE warteschlange) {
        this.nummer = nummer;
        this.warteschlange = warteschlange;
        warteschlange.KassennummerSetzen(nummer);
        darstellung = new KASSENSYMBOL();
        darstellung.PositionSetzen(nummer);
    }

    void KundeHolen() {
        if(kunde != null) kunde.KundensymbolEntfernen();
        kunde = warteschlange.ErstenKundenGeben();
        warteschlange.Aufruecken();
        if (kunde != null) kunde.NeuPositionieren(nummer, 0);
        System.out.println("Haben Sie eine Payback-Karte?");
    }

}

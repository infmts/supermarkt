/**
 * Created by beb on 2017-03-01.
 */
public class KUNDE {
    private KUNDENSYMBOL darstellung;
    private int artikelzahl;

    KUNDE(int artikelzahl) {
        this.artikelzahl = artikelzahl;
        darstellung = new KUNDENSYMBOL();
        darstellung.GroesseSetzen(artikelzahl);
        darstellung.PositionSetzen(1, 1);
        darstellung.FarbeSetzen("zufall");
    }

    void Antellen(WARTESCHLANGE schlange) {
        schlange.Einreihen(this);
    }

    void NeuPositionieren(int kassennummer, int kundennummer) {
        darstellung.PositionSetzen(kassennummer, kundennummer);
    }

    void KundensymbolEntfernen() {
        darstellung.Entfernen();
    }

}

/**
 * Created by beb on 2017-03-03.
 */
public class WARTESCHLANGE {
    private KUNDE kundeVorne;
    private KUNDE kundeHinten;
    private int kassennummer;

    WARTESCHLANGE() {

    }

    void Einreihen(KUNDE kunde) {
        if (kundeVorne == null) kundeVorne = kunde;
        else if (kundeHinten == null) kundeHinten = kunde;
    }

    KUNDE ErstenKundenGeben() {
        return kundeVorne;
    }

    void Aufruecken() {
        kundeVorne = kundeHinten;
        kundeHinten = null;

        KundenNeuPositionieren();
    }

    void KassennummerSetzen(int kassennummer) {
        this.kassennummer = kassennummer;
        KundenNeuPositionieren();
    }

    void KundenNeuPositionieren() {
        if (kundeVorne != null) kundeVorne.NeuPositionieren(kassennummer, 1);
        if (kundeHinten != null) kundeHinten.NeuPositionieren(kassennummer, 2);
    }
}
